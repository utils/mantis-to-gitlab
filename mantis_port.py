import gitlab

import csv
import urllib

class MantisIssues(object):
    def __init__(self, fin, url):
        self._issues = {}
        self._max_issue_id = -1

        for issue in csv.DictReader(fin):
            issue_id = int(issue['Id'])
            issue['Id'] = issue_id
            issue['__url'] = '%s/view.php?id=%d' % (url, issue_id)
            self._max_issue_id = max(issue_id, self._max_issue_id)
            self._issues[issue_id] = issue

        self._max_issue_id += 1
        if not self._max_issue_id:
            raise RuntimeError('no issues?')

        self._closed_statuses = set()
        self._users = {}
        self._categories = {}
        self._priorities = {}
        self._severities = {}
        self._projects = {}
        self._statuses = {}

    @property
    def max_id(self):
        return self._max_issue_id

    def add_category_mapping(self, category, labels):
        self._categories[category] = set(labels)

    def add_priority_mapping(self, priority, labels):
        self._priorities[priority] = set(labels)

    def add_severity_mapping(self, severity, labels):
        self._severities[severity] = set(labels)

    def add_project_mapping(self, project, labels):
        self._projects[project] = set(labels)

    def add_status_mapping(self, status, labels):
        self._statuses[status] = set(labels)

    def get_labels(self, issue):
        clabels = self._categories.get(issue.get('Category'), set())
        plabels = self._priorities.get(issue.get('Priority'), set())
        slabels = self._severities.get(issue.get('Severity'), set())
        jlabels = self._projects.get(issue.get('Project'), set())
        dlabels = self._statuses.get(issue.get('Status'), set())

        return clabels | plabels | slabels | jlabels | dlabels

    def add_user_mapping(self, name, gitlab_user):
        self._users[name] = gitlab_user

    def username(self, name):
        return self._users.get(name)

    def add_closed_status(self, status=None, resolution=None):
        if status is None and resolution is None:
            raise RuntimeError('at least a status or resolution must be provided')
        self._closed_statuses.add((status, resolution))

    def is_closed(self, issue):
        status, resolution = issue['Status'], issue.get('Resolution')

        for cstatus, cresolution in self._closed_statuses:
            smatch = cstatus     is None or cstatus     == status
            rmatch = cresolution is None or cresolution == resolution
            if smatch and rmatch:
                return True
        return False

    def __getitem__(self, issue_id):
        return self._issues.get(issue_id)

    def __contains__(self, issue_id):
        return issue_id in self._issues

class Mantis2Gitlab(object):
    def __init__(self, gl, project, user, dry_run=False):
        self.gl = gl
        self.user = user
        self._user_cache = {}
        self.dry_run = dry_run

        if not self.gl.currentuser():
            raise RuntimeError('invalid token?')

        project = self.gl.getproject(urllib.quote(project, ''))
        if not project:
            raise RuntimeError('target project does not exist!')
        self.project_id = project['id']
        if not self.dry_run and self.gl.getissues(self.project_id):
            raise RuntimeError('target project already contains issues!')

    def _get_user(self, username):
        if username not in self._user_cache:
            user = self.gl.getusers(username=username)
            if not user:
                raise RuntimeError('unknown user %s' % username)
            self._user_cache[username] = user[0]
        return self._user_cache[username]

    def _create_gitlab_issue(self, data):
        if self.dry_run:
            import pprint
            pprint.pprint(data)
            return {'id': -1}
        else:
            issue = self.gl.createissue(self.project_id, **data)
            if not issue:
                raise RuntimeError('failed to create issue %d' % issue_id)
            return issue

    def _create_issue(self, issues, issue):
        mantis_link_text = \
            '**This issue was created automatically ' \
            'from an original [Mantis Issue](%(__url)s). ' \
            'Further discussion may take place here.**' \
            % issue
        if 'Description' in issue:
            description = \
                mantis_link_text + \
                '\n\n' \
                '---\n\n' + \
                issue['Description']
        else:
            description = mantis_link_text
        issue_data = {
                'title': issue['Summary'],
                'description': description,
                'created_at': '%(Date Submitted)sT00:00:00Z' % issue,
                #'sudo': self.user,
            }

        is_closed = issues.is_closed(issue)
        if not is_closed and 'Assigned To' in issue:
            username = issues.username(issue['Assigned To'])
            if username:
                user = self._get_user(username)
                issue_data['assignee_id'] = user['id']

        labels = issues.get_labels(issue)
        issue_data['labels'] = ','.join(labels)

        print 'creating issue:', issue['Id']
        issue = self._create_gitlab_issue(issue_data)
        if is_closed:
            self._close_issue(issue)

    def _create_dummy_issue(self, issue_id):
        issue_data = {
                'title': 'Unused placeholder %d' % issue_id,
                'description': 'This issue was created automatically from an '
                               'original Mantis Issue that did not belong to '
                               'this project; please ignore it.',
                #'sudo': self.user,
            }

        issue = self._create_gitlab_issue(issue_data)
        self._delete_issue(issue)

    def _close_issue(self, issue):
        if self.dry_run:
            print 'would close the issue'
        else:
            if self.gl.editissue(self.project_id, issue['id'], state_event='close') is False:
                raise RuntimeError('failed to close issue %(iid)s' % issue)

    def _delete_issue(self, issue):
        if self.dry_run:
            print 'would delete the issue'
        else:
            if self.gl.deleteissue(self.project_id, issue['id']) is False:
                raise RuntimeError('failed to delete issue %(iid)s' % issue)

    def create_issues(self, issues, start_id=1):
        for issue_id in range(start_id, issues.max_id):
            if issue_id in issues:
                self._create_issue(issues, issues[issue_id])
            else:
                self._create_dummy_issue(issue_id)

def _argparser():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--issues', type=str, required=True,
                        help='csv file to read mantis issues from')
    parser.add_argument('-m', '--mantis-base', type=str, required=True,
                        help='base Mantis URL to use for linking back to Mantis')
    parser.add_argument('-g', '--gitlab', type=str, required=True,
                        help='gitlab base url')
    parser.add_argument('-p', '--project', type=str, required=True,
                        help='gitlab project')
    parser.add_argument('-t', '--token', type=str, required=True,
                        help='gitlab token')
    parser.add_argument('-u', '--user', type=str, required=True,
                        help='the gitlab user to user for all the issue creation')
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help='do not actually submit issues')

    #parser.add_argument('-s', '--start', type=int,
    #                    help='issue number to start importing')

    return parser

if __name__ == '__main__':
    parser = _argparser()
    args = parser.parse_args()

    with open(args.issues, 'r') as fin:
        mantis_issues = MantisIssues(fin, args.mantis_base)

    # To have users map from Mantis to GitLab, add users mappings:
    #    mantis_issues.add_user_mapping(mantis_display_name, gitlab_username)

    # To add labels based on the category, priority, severity, project, and
    # status:
    #    mantis_issues.add_category_mapping(category, ['label1', 'label2'])
    #    mantis_issues.add_priority_mapping(priority, ['label1', 'label2'])
    #    mantis_issues.add_severity_mapping(severity, ['label1', 'label2'])
    #    mantis_issues.add_project_mapping(project, ['label1', 'label2'])
    #    mantis_issues.add_status_mapping(status, ['label1', 'label2'])

    # To have status/resolution pairs cause issues to be closed, use:
    #   The 'closed' status will close regardless of the resolution:
    #    mantis_issues.add_closed_status(status='closed')
    #   The 'cantfix' resolution will close regardless of the status:
    #    mantis_issues.add_closed_status(resolution='cantfix')
    #   The 'backlog'/'expired' status/resolution will close the issue:
    #    mantis_issues.add_closed_status(status='backlog', resolution='expired')

    gl = gitlab.Gitlab(args.gitlab, args.token)
    m2gl = Mantis2Gitlab(gl, args.project, args.user, dry_run=args.dry_run)

    m2gl.create_issues(mantis_issues)
